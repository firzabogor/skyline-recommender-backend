from app.model.baselinemakanan import BaselineMakanan
from flask import request, jsonify
from app import response, db
from flask_jwt_extended import *


def index():
    try:
        baselinemakanan = BaselineMakanan.query.all()
        data = transform(baselinemakanan)
        return response.ok(data, "")
    except Exception as e:
        print(e)

def store():
    try:
        id_type_makanan = request.json['id_type_makanan']
        bahan_bumbu = request.json['bahan_bumbu']
        tekstur = request.json['tekstur']
        aroma = request.json['aroma']
        rasa = request.json['rasa']
        cara_memasak = request.json['cara_memasak']
        cara_penyajian = request.json['cara_penyajian']
        level_pedas = request.json['level_pedas']
        level_gurih = request.json['level_gurih']
        level_manis = request.json['level_manis']
        level_asin = request.json['level_asin']
        level_asam = request.json['level_asam']
        nutrisi = request.json['nutrisi']
        tekstur_level = request.json['tekstur_level']
        penggunaan_bahan_alami = request.json['penggunaan_bahan_alami']
        kesegaran_makanan = request.json['kesegaran_makanan']
        kebersihan_makanan = request.json['kebersihan_makanan']
        status_halal_makanan = request.json['status_halal_makanan']
        harga = request.json['harga']


        baselinemakanan = BaselineMakanan(id_type_makanan=id_type_makanan, bahan_bumbu=bahan_bumbu,
        tekstur=tekstur, aroma=aroma, rasa=rasa, cara_memasak=cara_memasak, cara_penyajian=cara_penyajian,
        level_pedas=level_pedas, level_gurih=level_gurih, level_manis=level_manis, level_asin=level_asin,
        level_asam=level_asam, nutrisi=nutrisi, tekstur_level=tekstur_level, penggunaan_bahan_alami=penggunaan_bahan_alami,
        kesegaran_makanan=kesegaran_makanan, kebersihan_makanan=kebersihan_makanan, status_halal_makanan=status_halal_makanan,
        harga=harga)
        db.session.add(baselinemakanan)
        db.session.commit()

        return response.ok('', 'Successfully create Baseline Makanan!')

    except Exception as e:
        print(e)


def update(id):
    try:
        id_type_makanan = request.json['id_type_makanan']
        bahan_bumbu = request.json['bahan_bumbu']
        tekstur = request.json['tekstur']
        aroma = request.json['aroma']
        rasa = request.json['rasa']
        cara_memasak = request.json['cara_memasak']
        cara_penyajian = request.json['cara_penyajian']
        level_pedas = request.json['level_pedas']
        level_gurih = request.json['level_gurih']
        level_manis = request.json['level_manis']
        level_asin = request.json['level_asin']
        level_asam = request.json['level_asam']
        nutrisi = request.json['nutrisi']
        tekstur_level = request.json['tekstur_level']
        penggunaan_bahan_alami = request.json['penggunaan_bahan_alami']
        kesegaran_makanan = request.json['kesegaran_makanan']
        kebersihan_makanan = request.json['kebersihan_makanan']
        status_halal_makanan = request.json['status_halal_makanan']
        harga = request.json['harga']

        baselinemakanan = BaselineMakanan.query.filter_by(id=id).first()
        baselinemakanan.id_type_makanan = id_type_makanan
        baselinemakanan.bahan_bumbu = bahan_bumbu
        baselinemakanan.tekstur = tekstur
        baselinemakanan.aroma = aroma
        baselinemakanan.rasa = rasa
        baselinemakanan.cara_memasak = cara_memasak
        baselinemakanan.cara_penyajian = cara_penyajian
        baselinemakanan.level_pedas = level_pedas
        baselinemakanan.level_gurih = level_gurih
        baselinemakanan.level_manis = level_manis
        baselinemakanan.level_asin = level_asin
        baselinemakanan.level_asam = level_asam
        baselinemakanan.nutrisi = nutrisi
        baselinemakanan.tekstur_level = tekstur_level
        baselinemakanan.penggunaan_bahan_alami = penggunaan_bahan_alami
        baselinemakanan.kesegaran_makanan = kesegaran_makanan
        baselinemakanan.kebersihan_makanan = kebersihan_makanan
        baselinemakanan.status_halal_makanan = status_halal_makanan
        baselinemakanan.harga = harga

        db.session.commit()

        return response.ok('', 'Successfully update Baseline Makanan!')

    except Exception as e:
        print(e)


def show(id):
    try:
        baselinemakanan = BaselineMakanan.query.filter_by(id=id).first()
        if not baselinemakanan:
            return response.badRequest([], 'Empty....')

        data = singleTransform(baselinemakanan)
        return response.ok(data, "")
    except Exception as e:
        print(e)


def delete(id):
    try:
        baselinemakanan = BaselineMakanan.query.filter_by(id=id).first()
        if not baselinemakanan:
            return response.badRequest([], 'Empty....')

        db.session.delete(baselinemakanan)
        db.session.commit()

        return response.ok('', 'Successfully delete Baseline Makanan!')
    except Exception as e:
        print(e)


def transform(values):
    array = []
    for i in values:
        array.append(singleTransform(i))
    return array


def singleTransform(values, withTypeMakanan=True):
    data = {
        'id_type_makanan' : values.id_type_makanan,
        'bahan_bumbu' : values.bahan_bumbu,
        'tekstur' : values.tekstur,
        'aroma' : values.aroma,
        'rasa' : values.rasa,
        'cara_memasak' : values.cara_memasak,
        'cara_penyajian' : values.cara_penyajian,
        'level_pedas' : values.level_pedas,
        'level_gurih' : values.level_gurih,
        'level_manis' : values.level_manis,
        'level_asin' : values.level_asin,
        'level_asam' : values.level_asam,
        'nutrisi' : values.nutrisi,
        'tekstur_level' : values.tekstur_level,
        'penggunaan_bahan_alami' : values.penggunaan_bahan_alami,
        'kesegaran_makanan' : values.kesegaran_makanan,
        'kebersihan_makanan' : values.kebersihan_makanan,
        'status_halal_makanan' : values.status_halal_makanan,
        'harga' : values.harga
    }
    
    if withTypeMakanan:
        typemakanan = []
        typemakanan.append({
            'id': values.type_makanan.id,
            'nama': values.type_makanan.nama,
            'kode_asal': values.type_makanan.kode_asal,
        })  
        data['typemakanan'] = typemakanan

    return data
