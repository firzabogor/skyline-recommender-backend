from app.model.wilayah import Wilayah
from flask import request, jsonify
from app import response, db
from app.controller import UserController
from flask_jwt_extended import *


def index():
    try:
        wilayah = Wilayah.query.all()
        data = transform(wilayah)
        return response.ok(data, "")
    except Exception as e:
        print(e)

def store():
    try:
        kode = request.json['kode']
        nama = request.json['nama']

        wilayah = Wilayah(kode=kode, nama=nama)
        db.session.add(wilayah)
        db.session.commit()

        return response.ok('', 'Successfully create wilayah!')

    except Exception as e:
        print(e)


def update(kode):
    try:
        kode = request.json['kode']
        nama = request.json['nama']

        wilayah = Wilayah.query.filter_by(kode=kode).first()
        wilayah.kode = kode
        wilayah.nama = nama

        db.session.commit()

        return response.ok('', 'Successfully update wilayah!')

    except Exception as e:
        print(e)


def show(kode):
    try:
        wilayah = Wilayah.query.filter_by(kode=kode).first()
        if not wilayah:
            return response.badRequest([], 'Empty....')

        data = singleTransform(wilayah)
        return response.ok(data, "")
    except Exception as e:
        print(e)


def delete(kode):
    try:
        wilayah = Wilayah.query.filter_by(kode=kode).first()
        if not wilayah:
            return response.badRequest([], 'Empty....')

        db.session.delete(wilayah)
        db.session.commit()

        return response.ok('', 'Successfully delete wilayah!')
    except Exception as e:
        print(e)


def transform(values):
    array = []
    for i in values:
        array.append(singleTransform(i))
    return array


def singleTransform(values, withTypeMakanan=True):

    data = {
        'kode': values.kode,
        'nama': values.nama
    }

    if withTypeMakanan:
        if values.type_makanan is None:
            print("None")
        elif hasattr(values.type_makanan, '__iter__'):
            type_makanan = []
            for i in values.type_makanan:
                type_makanan.append({
                    'id': i.id,
                    'kode_asal': i.kode_asal,
                    'nama': i.nama,
                })
            data['type_makanan'] = type_makanan
            if values.type_makanan.baseline_makanan is None:
                print("None")
            elif hasattr(values.type_makanan.baseline_makanan, '__iter__'):
                baseline_makanan = []
                for i in values.type_makanan.baseline_makanan:
                    baseline_makanan.append({
                        'id': i.id,
                        'id_type_makanan': i.id_type_makanan,
                        'bahan_bumbu' : i.bahan_bumbu,
                        'tekstur' : i.tekstur,
                        'aroma' : i.aroma,
                        'rasa' : i.rasa,
                        'cara_memasak' : i.cara_memasak,
                        'cara_penyajian' : i.cara_penyajian,
                        'level_pedas' : i.level_pedas,
                        'level_gurih' : i.level_gurih,
                        'level_manis' : i.level_manis,
                        'level_asin' : i.level_asin,
                        'level_asam' : i.level_asam,
                        'nutrisi' : i.nutrisi,
                        'tekstur_level' : i.tekstur_level,
                        'penggunaan_bahan_alami' : i.penggunaan_bahan_alami,
                        'kesegaran_makanan' : i.kesegaran_makanan,
                        'kebersihan_makanan' : i.kebersihan_makanan,
                        'status_halal_makanan' : i.status_halal_makanan,
                        'harga' : i.harga,
                    })
                data['baseline_makanan'] = baseline_makanan
            else:
                baseline_makanan = []
                i = values.type_makanan
                baseline_makanan.append({
                        'id': values.type_makanan.id,
                        'id_type_makanan': i.id_type_makanan,
                        'bahan_bumbu' : i.bahan_bumbu,
                        'tekstur' : i.tekstur,
                        'aroma' : i.aroma,
                        'rasa' : i.rasa,
                        'cara_memasak' : i.cara_memasak,
                        'cara_penyajian' : i.cara_penyajian,
                        'level_pedas' : i.level_pedas,
                        'level_gurih' : i.level_gurih,
                        'level_manis' : i.level_manis,
                        'level_asin' : i.level_asin,
                        'level_asam' : i.level_asam,
                        'nutrisi' : i.nutrisi,
                        'tekstur_level' : i.tekstur_level,
                        'penggunaan_bahan_alami' : i.penggunaan_bahan_alami,
                        'kesegaran_makanan' : i.kesegaran_makanan,
                        'kebersihan_makanan' : i.kebersihan_makanan,
                        'status_halal_makanan' : i.status_halal_makanan,
                        'harga' : i.harga,
                    })
                data['baseline_makanan'] = baseline_makanan
        else:
            type_makanan = []
            type_makanan.append({
                'id': values.type_makanan.id,
                'kode_asal': values.type_makanan.kode_asal,
                'nama': values.type_makanan.nama,
            })
            data['type_makanan'] = type_makanan
            if values.type_makanan.baseline_makanan is None:
                print("None")
            elif hasattr(values.type_makanan.baseline_makanan, '__iter__'):
                baseline_makanan = []
                for i in values.type_makanan.baseline_makanan:
                    baseline_makanan.append({
                        'id': i.id,
                        'id_type_makanan': i.id_type_makanan,
                        'bahan_bumbu' : i.bahan_bumbu,
                        'tekstur' : i.tekstur,
                        'aroma' : i.aroma,
                        'rasa' : i.rasa,
                        'cara_memasak' : i.cara_memasak,
                        'cara_penyajian' : i.cara_penyajian,
                        'level_pedas' : i.level_pedas,
                        'level_gurih' : i.level_gurih,
                        'level_manis' : i.level_manis,
                        'level_asin' : i.level_asin,
                        'level_asam' : i.level_asam,
                        'nutrisi' : i.nutrisi,
                        'tekstur_level' : i.tekstur_level,
                        'penggunaan_bahan_alami' : i.penggunaan_bahan_alami,
                        'kesegaran_makanan' : i.kesegaran_makanan,
                        'kebersihan_makanan' : i.kebersihan_makanan,
                        'status_halal_makanan' : i.status_halal_makanan,
                        'harga' : i.harga,
                    })
                data['baseline_makanan'] = baseline_makanan
            else:
                baseline_makanan = []
                i = values.type_makanan
                baseline_makanan.append({
                        'id': values.type_makanan.id,
                        'id_type_makanan': i.id_type_makanan,
                        'bahan_bumbu' : i.bahan_bumbu,
                        'tekstur' : i.tekstur,
                        'aroma' : i.aroma,
                        'rasa' : i.rasa,
                        'cara_memasak' : i.cara_memasak,
                        'cara_penyajian' : i.cara_penyajian,
                        'level_pedas' : i.level_pedas,
                        'level_gurih' : i.level_gurih,
                        'level_manis' : i.level_manis,
                        'level_asin' : i.level_asin,
                        'level_asam' : i.level_asam,
                        'nutrisi' : i.nutrisi,
                        'tekstur_level' : i.tekstur_level,
                        'penggunaan_bahan_alami' : i.penggunaan_bahan_alami,
                        'kesegaran_makanan' : i.kesegaran_makanan,
                        'kebersihan_makanan' : i.kebersihan_makanan,
                        'status_halal_makanan' : i.status_halal_makanan,
                        'harga' : i.harga,
                    })
                data['baseline_makanan'] = baseline_makanan

    return data