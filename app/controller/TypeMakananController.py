from app.model.typemakanan import TypeMakanan
from flask import request, jsonify
from app import response, db
from flask_jwt_extended import *


def index():
    try:
        typemakanan = TypeMakanan.query.all()
        data = transform(typemakanan)
        return response.ok(data, "")
    except Exception as e:
        print(e)

def store():
    try:
        nama = request.json['nama']
        kode_asal = request.json['kode_asal']

        typemakanan = TypeMakanan(kode_asal=kode_asal, nama=nama)
        db.session.add(typemakanan)
        db.session.commit()

        return response.ok('', 'Successfully create type makanan!')

    except Exception as e:
        print(e)


def update(id):
    try:
        nama = request.json['nama']
        kode_asal = request.json['kode_asal']

        typemakanan = TypeMakanan.query.filter_by(id=id).first()
        typemakanan.kode = kode_asal
        typemakanan.nama = nama

        db.session.commit()

        return response.ok('', 'Successfully update type makanan!')

    except Exception as e:
        print(e)


def show(id):
    try:
        typemakanan = TypeMakanan.query.filter_by(id=id).first()
        if not typemakanan:
            return response.badRequest([], 'Empty....')

        data = singleTransform(typemakanan)
        return response.ok(data, "")
    except Exception as e:
        print(e)


def delete(id):
    try:
        typemakanan = TypeMakanan.query.filter_by(id=id).first()
        if not typemakanan:
            return response.badRequest([], 'Empty....')

        db.session.delete(typemakanan)
        db.session.commit()

        return response.ok('', 'Successfully delete type makanan!')
    except Exception as e:
        print(e)


def transform(values):
    array = []
    for i in values:
        array.append(singleTransform(i))
    return array


def singleTransform(values):
    data = {
        'kode_asal': values.kode_asal,
        'nama': values.nama
    }

    return data