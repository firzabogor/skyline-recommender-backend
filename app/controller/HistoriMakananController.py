from app.model.historimakanan import HistoriMakanan
from flask import request, jsonify
from app import response, db
from flask_jwt_extended import *


def index():
    try:
        historimakanan = HistoriMakanan.query.all()
        data = transform(historimakanan)
        return response.ok(data, "")
    except Exception as e:
        print(e)

def store():
    try:
        waktu_pemesanan = request.json['waktu_pemesanan']
        id_profil_makanan = request.json['id_profil_makanan']
        rating = request.json['rating']

        historimakanan = HistoriMakanan(user_id=user_id, todo=todo, description=desc)
        db.session.add(historimakanan)
        db.session.commit()

        return response.ok('', 'Successfully create Histori Makanan!')

    except Exception as e:
        print(e)


def update(id):
    try:
        waktu_pemesanan = request.json['waktu_pemesanan']
        id_profil_makanan = request.json['id_profil_makanan']
        rating = request.json['rating']

        historimakanan = HistoriMakanan.query.filter_by(id=id).first()
        historimakanan.waktu_pemesanan = waktu_pemesanan
        historimakanan.id_profil_makanan = id_profil_makanan
        historimakanan.rating = rating

        db.session.commit()

        return response.ok('', 'Successfully update Histori Makanan!')

    except Exception as e:
        print(e)


def show(id):
    try:
        historimakanan = HistoriMakanan.query.filter_by(id=id).first()
        if not historimakanan:
            return response.badRequest([], 'Empty....')

        data = singleTransform(historimakanan)
        return response.ok(data, "")
    except Exception as e:
        print(e)


def delete(id):
    try:
        historimakanan = HistoriMakanan.query.filter_by(id=id).first()
        if not historimakanan:
            return response.badRequest([], 'Empty....')

        db.session.delete(historimakanan)
        db.session.commit()

        return response.ok('', 'Successfully delete data!')
    except Exception as e:
        print(e)


def transform(values):
    array = []
    for i in values:
        array.append(singleTransform(i))
    return array


def singleTransform(values):
    data = {
        'waktu_pemesanan': values.waktu_pemesanan,
        'id_profil_makanan': values.id_profil_makanan,
        'rating': values.rating
    }

    return data