from app.model.profilmakanan import ProfilMakanan
from app.model.restoran import Restoran
from app.model.typemakanan import TypeMakanan
from flask import request, jsonify
from app import response, db
from flask_jwt_extended import *


def index():
    try:
        profilmakanan = ProfilMakanan.query.all()
        data = transform(profilmakanan)
        return response.ok(data, "")
    except Exception as e:
        print(e)

def store():
    try:
        id_type_makanan = request.json['id_type_makanan']
        id_restoran = request.json['id_restoran']
        bahan_bumbu = request.json['bahan_bumbu']
        tekstur = request.json['tekstur']
        aroma = request.json['aroma']
        rasa = request.json['rasa']
        cara_memasak = request.json['cara_memasak']
        cara_penyajian = request.json['cara_penyajian']
        level_pedas = request.json['level_pedas']
        level_gurih = request.json['level_gurih']
        level_manis = request.json['level_manis']
        level_asin = request.json['level_asin']
        level_asam = request.json['level_asam']
        nutrisi = request.json['nutrisi']
        tekstur_level = request.json['tekstur_level']
        penggunaan_bahan_alami = request.json['penggunaan_bahan_alami']
        kesegaran_makanan = request.json['kesegaran_makanan']
        kebersihan_makanan = request.json['kebersihan_makanan']
        status_halal_makanan = request.json['status_halal_makanan']
        harga = request.json['harga']


        profilmakanan = ProfilMakanan(id_type_makanan=id_type_makanan, bahan_bumbu=bahan_bumbu, id_restoran=id_restoran,
        tekstur=tekstur, aroma=aroma, rasa=rasa, cara_memasak=cara_memasak, cara_penyajian=cara_penyajian,
        level_pedas=level_pedas, level_gurih=level_gurih, level_manis=level_manis, level_asin=level_asin,
        level_asam=level_asam, nutrisi=nutrisi, tekstur_level=tekstur_level, penggunaan_bahan_alami=penggunaan_bahan_alami,
        kesegaran_makanan=kesegaran_makanan, kebersihan_makanan=kebersihan_makanan, status_halal_makanan=status_halal_makanan,
        harga=harga)
        db.session.add(profilmakanan)
        db.session.commit()

        return response.ok('', 'Successfully create Profil Makanan!')

    except Exception as e:
        print(e)

def rating(id):
    try:
        rating = request.json['rating']
        hit = request.json['hit']

        profilmakanan = ProfilMakanan.query.filter_by(id=id).first()
        profilmakanan.rating = rating
        profilmakanan.hit = hit

        db.session.commit()

        return response.ok('', 'Successfully update Profil Makanan!')

    except Exception as e:
        print(e)


def update(id):
    try:
        id_type_makanan = request.json['id_type_makanan']
        id_restoran = request.json['id_restoran']
        bahan_bumbu = request.json['bahan_bumbu']
        tekstur = request.json['tekstur']
        aroma = request.json['aroma']
        rasa = request.json['rasa']
        cara_memasak = request.json['cara_memasak']
        cara_penyajian = request.json['cara_penyajian']
        level_pedas = request.json['level_pedas']
        level_gurih = request.json['level_gurih']
        level_manis = request.json['level_manis']
        level_asin = request.json['level_asin']
        level_asam = request.json['level_asam']
        nutrisi = request.json['nutrisi']
        tekstur_level = request.json['tekstur_level']
        penggunaan_bahan_alami = request.json['penggunaan_bahan_alami']
        kesegaran_makanan = request.json['kesegaran_makanan']
        kebersihan_makanan = request.json['kebersihan_makanan']
        status_halal_makanan = request.json['status_halal_makanan']
        harga = request.json['harga']

        profilmakanan = ProfilMakanan.query.filter_by(id=id).first()
        profilmakanan.id_type_makanan = id_type_makanan
        profilmakanan.id_restoran = id_restoran
        profilmakanan.bahan_bumbu = bahan_bumbu
        profilmakanan.tekstur = tekstur
        profilmakanan.aroma = aroma
        profilmakanan.rasa = rasa
        profilmakanan.cara_memasak = cara_memasak
        profilmakanan.cara_penyajian = cara_penyajian
        profilmakanan.level_pedas = level_pedas
        profilmakanan.level_gurih = level_gurih
        profilmakanan.level_manis = level_manis
        profilmakanan.level_asin = level_asin
        profilmakanan.level_asam = level_asam
        profilmakanan.nutrisi = nutrisi
        profilmakanan.tekstur_level = tekstur_level
        profilmakanan.penggunaan_bahan_alami = penggunaan_bahan_alami
        profilmakanan.kesegaran_makanan = kesegaran_makanan
        profilmakanan.kebersihan_makanan = kebersihan_makanan
        profilmakanan.status_halal_makanan = status_halal_makanan
        profilmakanan.harga = harga

        db.session.commit()

        return response.ok('', 'Successfully update Profil Makanan!')

    except Exception as e:
        print(e)


def show(id):
    try:
        profilmakanan = ProfilMakanan.query.join(Restoran, ProfilMakanan.id_restoran == Restoran.id).filter_by(id=id).first()
        if not profilmakanan:
            return response.badRequest([], 'Empty....')

        data = singleTransformJoinRestoran(profilmakanan)
        return response.ok(data, "")
    except Exception as e:
        print(e)


def delete(id):
    try:
        profilmakanan = ProfilMakanan.query.filter_by(id=id).first()
        if not profilmakanan:
            return response.badRequest([], 'Empty....')

        db.session.delete(profilmakanan)
        db.session.commit()

        return response.ok('', 'Successfully delete Profil Makanan!')
    except Exception as e:
        print(e)


def transform(values):
    array = []
    for i in values:
        array.append(singleTransform(i))
    return array

def transform2(values):
    array = []
    for i in values:
        array.append(singleTransformJoinRestoran(i))
    return array

def byRating(id):
    try:
        profilmakanan = ProfilMakanan.query.filter_by(id_type_makanan=id).order_by(ProfilMakanan.rating.desc()).first()
        data = singleTransformJoinRestoran(profilmakanan)
        return response.ok(data, "")
    except Exception as e:
        print(e)

def byType(id):
    try:
        profilmakanan = ProfilMakanan.query.filter_by(id_type_makanan=id)
        data = transform2(profilmakanan)
        return response.ok(data, "")
    except Exception as e:
        print(e)

def singleTransformJoinRestoran(values):
    data = {
        'id' : values.id,
        'id_type_makanan' : values.id_type_makanan,
        'id_restoran' : values.id_restoran,
        'bahan_bumbu' : values.bahan_bumbu,
        'tekstur' : values.tekstur,
        'aroma' : values.aroma,
        'rasa' : values.rasa,
        'cara_memasak' : values.cara_memasak,
        'cara_penyajian' : values.cara_penyajian,
        'level_pedas' : values.level_pedas,
        'level_gurih' : values.level_gurih,
        'level_manis' : values.level_manis,
        'level_asin' : values.level_asin,
        'level_asam' : values.level_asam,
        'nutrisi' : values.nutrisi,
        'tekstur_level' : values.tekstur_level,
        'penggunaan_bahan_alami' : values.penggunaan_bahan_alami,
        'kesegaran_makanan' : values.kesegaran_makanan,
        'kebersihan_makanan' : values.kebersihan_makanan,
        'status_halal_makanan' : values.status_halal_makanan,
        'harga' : values.harga,
        'nama_restoran' : values.restoran.nama,
        'alamat' : values.restoran.alamat,
        'no_telp' : values.restoran.no_telp,
        'type_makanan' : values.type_makanan.nama,
        'rating' : values.rating,
        'hit' : values.hit
    }

    return data


def singleTransform(values):
    data = {
        'id_type_makanan' : values.id_type_makanan,
        'id_restoran' : values.id_restoran,
        'bahan_bumbu' : values.bahan_bumbu,
        'tekstur' : values.tekstur,
        'aroma' : values.aroma,
        'rasa' : values.rasa,
        'cara_memasak' : values.cara_memasak,
        'cara_penyajian' : values.cara_penyajian,
        'level_pedas' : values.level_pedas,
        'level_gurih' : values.level_gurih,
        'level_manis' : values.level_manis,
        'level_asin' : values.level_asin,
        'level_asam' : values.level_asam,
        'nutrisi' : values.nutrisi,
        'tekstur_level' : values.tekstur_level,
        'penggunaan_bahan_alami' : values.penggunaan_bahan_alami,
        'kesegaran_makanan' : values.kesegaran_makanan,
        'kebersihan_makanan' : values.kebersihan_makanan,
        'status_halal_makanan' : values.status_halal_makanan,
        'harga' : values.harga,
    }

    return data