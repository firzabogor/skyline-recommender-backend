from app.model.user import Users
from app import response, app, db
from flask import request
from datetime import datetime
from datetime import timedelta
from app.controller import CustomerController
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,create_refresh_token,
    get_jwt_identity,jwt_refresh_token_required
)

def index():
    try:
        users = Users.query.all()
        data = transform(users)
        return response.ok(data, "")
    except Exception as e:
        print(e)

def show(id):
    try:
        users = Users.query.filter_by(id=id).first()
        if not users:
            return response.badRequest([], 'Empty....')

        data = singleTransform(users)
        return response.ok(data, "")
    except Exception as e:
        print(e)

def show_username():
    try:
        username = request.json['username']
        users = Users.query.filter_by(username=username).first()
        if not users:
            return response.badRequest([], 'Empty....')

        data = singleTransform(users)
        return response.ok(data, "")
    except Exception as e:
        print(e)

def singleTransform(users, withExtra=True):
    data = {
        'id': users.id,
        'username': users.username,
        'email': users.email,
        'type_user': users.type_user,
    }

    if withExtra:
        if users.type_user == 'c':
            customer = []
            customer.append({
                'id': users.customer.id,
                'kode_asal': users.customer.kode_asal,
                'nama': users.customer.nama,
                'no_telp': users.customer.no_telp,
                'alamat': users.customer.alamat,
                'id_user': users.customer.id_user,
            })
            data['customer'] = customer
        
        elif users.type_user == 'r':
            restoran = []
            restoran.append({
                'id': users.restoran.id,
                'nama': users.restoran.nama,
                'no_telp': users.restoran.no_telp,
                'alamat': users.restoran.alamat,
                'longitude': users.restoran.longitude,
                'latitude': users.restoran.latitude,
                'id_user': users.restoran.id_user,
            })  
            data['restoran'] = restoran

    return data

def transform(users):
    array = []
    for i in users:
        array.append(singleTransform(i))
    return array

def store():
    try:
        username = request.json['username']
        email = request.json['email']
        password = request.json['password']
        type_user = request.json['type_user']

        users = Users(username=username, email=email, type_user=type_user)
        users.setPassword(password)
        db.session.add(users)
        db.session.commit()

        return response.ok('', 'Successfully create data!')

    except Exception as e:
        print(e)

def update(id):
    try:
        username = request.json['username']
        email = request.json['email']
        password = request.json['password']
        type_user = request.json['type_user']

        user = Users.query.filter_by(id=id).first()
        user.email = email
        user.username = username
        user.setPassword(password)
        user.type_user = type_user

        db.session.commit()

        return response.ok('', 'Successfully update data!')

    except Exception as e:
        print(e)

def setPassword(self, password):
    self.password = generate_password_hash(password)

def checkPassword(self, password):
    return check_password_hash(self.password, password)


def delete(id):
    try:
        user = Users.query.filter_by(id=id).first()
        if not user:
            return response.badRequest([], 'Empty....')

        db.session.delete(user)
        db.session.commit()

        return response.ok('', 'Successfully delete data!')
    except Exception as e:
        print(e)

def login():
    try:
        username = request.json['username']
        password = request.json['password']

        user = Users.query.filter_by(username=username).first()
        if not user:
            return response.badRequest([], 'Empty....')

        if not user.checkPassword(password):
            return response.badRequest([], 'Your credentials is invalid')

        data = singleTransform(user, withExtra=False)
        expires = timedelta(days=1)
        expires_refresh = timedelta(days=3)
        access_token = create_access_token(data, fresh=True, expires_delta=expires)
        refresh_token = create_refresh_token(data, expires_delta=expires_refresh)

        return response.ok({
            "data": data,
            "token_access": access_token,
            "token_refresh": refresh_token,
        }, "")
    except Exception as e:
        print(e)

def login2():
    try:
        username = request.json['username']
        password = request.json['password']

        user = Users.query.filter_by(username=username).first()
        if not user:
            return response.badRequest([], 'Empty....')

        if not user.checkPassword(password):
            return response.badRequest([], 'Your credentials is invalid')

        data = singleTransform(user, withExtra=True)
        expires = timedelta(days=1)
        expires_refresh = timedelta(days=3)
        access_token = create_access_token(data, fresh=True, expires_delta=expires)
        refresh_token = create_refresh_token(data, expires_delta=expires_refresh)

        return response.ok({
            "data": data,
            "token_access": access_token,
            "token_refresh": refresh_token,
        }, "")
    except Exception as e:
        print(e)

@jwt_refresh_token_required
def refresh():
    try:
        user = get_jwt_identity()
        new_token = create_access_token(identity=user, fresh=False)

        return response.ok({
            "token_access": new_token
        }, "")

    except Exception as e:
        print(e)