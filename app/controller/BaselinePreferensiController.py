from app.model.baselinepreferensi import BaselinePreferensi
from flask import request, jsonify
from app import response, db
from flask_jwt_extended import *


def index():
    try:
        baselinepreferensi = BaselinePreferensi.query.all()
        data = transform(baselinepreferensi)
        return response.ok(data, "")
    except Exception as e:
        print(e)

def store():
    try:
        id_type_makanan = request.json['id_type_makanan']
        id_customer = request.json['id_customer']
        bahan_bumbu = request.json['bahan_bumbu']
        tekstur = request.json['tekstur']
        aroma = request.json['aroma']
        rasa = request.json['rasa']
        cara_memasak = request.json['cara_memasak']
        cara_penyajian = request.json['cara_penyajian']
        level_pedas = request.json['level_pedas']
        level_gurih = request.json['level_gurih']
        level_manis = request.json['level_manis']
        level_asin = request.json['level_asin']
        level_asam = request.json['level_asam']
        nutrisi = request.json['nutrisi']
        tekstur_level = request.json['tekstur_level']
        penggunaan_bahan_alami = request.json['penggunaan_bahan_alami']
        kesegaran_makanan = request.json['kesegaran_makanan']
        kebersihan_makanan = request.json['kebersihan_makanan']
        status_halal_makanan = request.json['status_halal_makanan']
        harga = request.json['harga']


        baselinepreferensi = BaselinePreferensi(id_type_makanan=id_type_makanan, bahan_bumbu=bahan_bumbu, id_customer=id_customer,
        tekstur=tekstur, aroma=aroma, rasa=rasa, cara_memasak=cara_memasak, cara_penyajian=cara_penyajian,
        level_pedas=level_pedas, level_gurih=level_gurih, level_manis=level_manis, level_asin=level_asin,
        level_asam=level_asam, nutrisi=nutrisi, tekstur_level=tekstur_level, penggunaan_bahan_alami=penggunaan_bahan_alami,
        kesegaran_makanan=kesegaran_makanan, kebersihan_makanan=kebersihan_makanan, status_halal_makanan=status_halal_makanan,
        harga=harga)
        db.session.add(baselinepreferensi)
        db.session.commit()

        return response.ok('', 'Successfully create Baseline Preferensi!')

    except Exception as e:
        print(e)


def update(id):
    try:
        id_type_makanan = request.json['id_type_makanan']
        id_customer = request.json['id_customer']
        bahan_bumbu = request.json['bahan_bumbu']
        tekstur = request.json['tekstur']
        aroma = request.json['aroma']
        rasa = request.json['rasa']
        cara_memasak = request.json['cara_memasak']
        cara_penyajian = request.json['cara_penyajian']
        level_pedas = request.json['level_pedas']
        level_gurih = request.json['level_gurih']
        level_manis = request.json['level_manis']
        level_asin = request.json['level_asin']
        level_asam = request.json['level_asam']
        nutrisi = request.json['nutrisi']
        tekstur_level = request.json['tekstur_level']
        penggunaan_bahan_alami = request.json['penggunaan_bahan_alami']
        kesegaran_makanan = request.json['kesegaran_makanan']
        kebersihan_makanan = request.json['kebersihan_makanan']
        status_halal_makanan = request.json['status_halal_makanan']
        harga = request.json['harga']

        baselinepreferensi = BaselinePreferensi.query.filter_by(id=id).first()
        baselinepreferensi.id_customer = id_customer
        baselinepreferensi.id_type_makanan = id_type_makanan
        baselinepreferensi.bahan_bumbu = bahan_bumbu
        baselinepreferensi.tekstur = tekstur
        baselinepreferensi.aroma = aroma
        baselinepreferensi.rasa = rasa
        baselinepreferensi.cara_memasak = cara_memasak
        baselinepreferensi.cara_penyajian = cara_penyajian
        baselinepreferensi.level_pedas = level_pedas
        baselinepreferensi.level_gurih = level_gurih
        baselinepreferensi.level_manis = level_manis
        baselinepreferensi.level_asin = level_asin
        baselinepreferensi.level_asam = level_asam
        baselinepreferensi.nutrisi = nutrisi
        baselinepreferensi.tekstur_level = tekstur_level
        baselinepreferensi.penggunaan_bahan_alami = penggunaan_bahan_alami
        baselinepreferensi.kesegaran_makanan = kesegaran_makanan
        baselinepreferensi.kebersihan_makanan = kebersihan_makanan
        baselinepreferensi.status_halal_makanan = status_halal_makanan
        baselinepreferensi.harga = harga

        db.session.commit()

        return response.ok('', 'Successfully update Baseline Preferensi!')

    except Exception as e:
        print(e)


def show(id):
    try:
        baselinepreferensi = BaselinePreferensi.query.filter_by(id=id).first()
        if not baselinepreferensi:
            return response.badRequest([], 'Empty....')

        data = singleTransform(baselinepreferensi)
        return response.ok(data, "")
    except Exception as e:
        print(e)


def insertupdatepreferensi():
    try:
        id_customer2 = request.json['id_customer']
        id_type_makanan2 = request.json['id_type_makanan']

        baselinepreferensi = BaselinePreferensi.query.filter_by(id_customer=id_customer2, id_type_makanan=id_type_makanan2).first()

        if not baselinepreferensi:
            id_type_makanan = request.json['id_type_makanan']
            id_customer = request.json['id_customer']
            bahan_bumbu = request.json['bahan_bumbu']
            tekstur = request.json['tekstur']
            aroma = request.json['aroma']
            rasa = request.json['rasa']
            cara_memasak = request.json['cara_memasak']
            cara_penyajian = request.json['cara_penyajian']
            level_pedas = request.json['level_pedas']
            level_gurih = request.json['level_gurih']
            level_manis = request.json['level_manis']
            level_asin = request.json['level_asin']
            level_asam = request.json['level_asam']
            nutrisi = request.json['nutrisi']
            tekstur_level = request.json['tekstur_level']
            penggunaan_bahan_alami = request.json['penggunaan_bahan_alami']
            kesegaran_makanan = request.json['kesegaran_makanan']
            kebersihan_makanan = request.json['kebersihan_makanan']
            status_halal_makanan = request.json['status_halal_makanan']
            harga = request.json['harga']


            baselinepreferensi = BaselinePreferensi(id_type_makanan=id_type_makanan, bahan_bumbu=bahan_bumbu, id_customer=id_customer,
            tekstur=tekstur, aroma=aroma, rasa=rasa, cara_memasak=cara_memasak, cara_penyajian=cara_penyajian,
            level_pedas=level_pedas, level_gurih=level_gurih, level_manis=level_manis, level_asin=level_asin,
            level_asam=level_asam, nutrisi=nutrisi, tekstur_level=tekstur_level, penggunaan_bahan_alami=penggunaan_bahan_alami,
            kesegaran_makanan=kesegaran_makanan, kebersihan_makanan=kebersihan_makanan, status_halal_makanan=status_halal_makanan,
            harga=harga)
            db.session.add(baselinepreferensi)
            db.session.commit()

            return response.ok('', 'Successfully create Baseline Preferensi!')

        else:
            id_type_makanan = request.json['id_type_makanan']
            id_customer = request.json['id_customer']
            bahan_bumbu = request.json['bahan_bumbu']
            tekstur = request.json['tekstur']
            aroma = request.json['aroma']
            rasa = request.json['rasa']
            cara_memasak = request.json['cara_memasak']
            cara_penyajian = request.json['cara_penyajian']
            level_pedas = request.json['level_pedas']
            level_gurih = request.json['level_gurih']
            level_manis = request.json['level_manis']
            level_asin = request.json['level_asin']
            level_asam = request.json['level_asam']
            nutrisi = request.json['nutrisi']
            tekstur_level = request.json['tekstur_level']
            penggunaan_bahan_alami = request.json['penggunaan_bahan_alami']
            kesegaran_makanan = request.json['kesegaran_makanan']
            kebersihan_makanan = request.json['kebersihan_makanan']
            status_halal_makanan = request.json['status_halal_makanan']
            harga = request.json['harga']

            baselinepreferensi.id_customer = id_customer
            baselinepreferensi.id_type_makanan = id_type_makanan
            baselinepreferensi.bahan_bumbu = bahan_bumbu
            baselinepreferensi.tekstur = tekstur
            baselinepreferensi.aroma = aroma
            baselinepreferensi.rasa = rasa
            baselinepreferensi.cara_memasak = cara_memasak
            baselinepreferensi.cara_penyajian = cara_penyajian
            baselinepreferensi.level_pedas = level_pedas
            baselinepreferensi.level_gurih = level_gurih
            baselinepreferensi.level_manis = level_manis
            baselinepreferensi.level_asin = level_asin
            baselinepreferensi.level_asam = level_asam
            baselinepreferensi.nutrisi = nutrisi
            baselinepreferensi.tekstur_level = tekstur_level
            baselinepreferensi.penggunaan_bahan_alami = penggunaan_bahan_alami
            baselinepreferensi.kesegaran_makanan = kesegaran_makanan
            baselinepreferensi.kebersihan_makanan = kebersihan_makanan
            baselinepreferensi.status_halal_makanan = status_halal_makanan
            baselinepreferensi.harga = harga

            db.session.commit()

            return response.ok('', 'Successfully update Baseline Preferensi!')
            
    except Exception as e:
        print(e)


def delete(id):
    try:
        baselinepreferensi = BaselinePreferensi.query.filter_by(id=id).first()
        if not baselinepreferensi:
            return response.badRequest([], 'Empty....')

        db.session.delete(baselinepreferensi)
        db.session.commit()

        return response.ok('', 'Successfully delete Baseline Preferensi!')
    except Exception as e:
        print(e)


def transform(values):
    array = []
    for i in values:
        array.append(singleTransform(i))
    return array


def singleTransform(values):
    data = {
        'id_type_makanan' : values.id_type_makanan,
        'id_customer' : values.id_customer,
        'bahan_bumbu' : values.bahan_bumbu,
        'tekstur' : values.tekstur,
        'aroma' : values.aroma,
        'rasa' : values.rasa,
        'cara_memasak' : values.cara_memasak,
        'cara_penyajian' : values.cara_penyajian,
        'level_pedas' : values.level_pedas,
        'level_gurih' : values.level_gurih,
        'level_manis' : values.level_manis,
        'level_asin' : values.level_asin,
        'level_asam' : values.level_asam,
        'nutrisi' : values.nutrisi,
        'tekstur_level' : values.tekstur_level,
        'penggunaan_bahan_alami' : values.penggunaan_bahan_alami,
        'kesegaran_makanan' : rvalues.kesegaran_makanan,
        'kebersihan_makanan' : values.kebersihan_makanan,
        'status_halal_makanan' : values.status_halal_makanan,
        'harga' : values.harga
    }

    return data