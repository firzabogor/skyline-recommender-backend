from app.model.restoran import Restoran
from flask import request, jsonify
from app import response, db
from app.controller import UserController
from flask_jwt_extended import *


@jwt_required
def index():
    try:
        id = request.args.get('id_user')
        restoran = Restoran.query.filter_by(id_user=id).all()
        data = transform(restoran)
        return response.ok(data, "")
    except Exception as e:
        print(e)

def store():
    try:
        id_user = request.json['id_user']
        nama = request.json['nama']
        no_telp = request.json['no_telp']
        alamat = request.json['alamat']
        longitude = request.json['longitude']
        latitude = request.json['latitude']

        restoran = Restoran(id_user=id_user, longitude=longitude, latitude=latitude , nama=nama, no_telp=no_telp, alamat=alamat)
        db.session.add(restoran)
        db.session.commit()

        return response.ok('', 'Successfully create restoran!')

    except Exception as e:
        print(e)


def update(id):
    try:
        id_user = request.json['id_user']
        nama = request.json['nama']
        no_telp = request.json['no_telp']
        alamat = request.json['alamat']
        longitude = request.json['longitude']
        latitude = request.json['latitude']

        restoran = Restoran.query.filter_by(id=id).first()
        restoran.id_user = id_user
        restoran.nama = nama
        restoran.no_telp = no_telp
        restoran.alamat = alamat
        restoran.longitude = longitude
        restoran.latitude = latitude

        db.session.commit()

        return response.ok('', 'Successfully update restoran!')

    except Exception as e:
        print(e)


def show(id):
    try:
        restoran = Restoran.query.filter_by(id=id).first()
        if not restoran:
            return response.badRequest([], 'Empty....')

        data = singleTransform(restoran)
        return response.ok(data, "")
    except Exception as e:
        print(e)


def delete(id):
    try:
        restoran = Restoran.query.filter_by(id=id).first()
        if not restoran:
            return response.badRequest([], 'Empty....')

        db.session.delete(restoran)
        db.session.commit()

        return response.ok('', 'Successfully delete restoran!')
    except Exception as e:
        print(e)


def transform(values):
    array = []
    for i in values:
        array.append(singleTransform(i))
    return array


def singleTransform(values):
    data = {
        'id': values.id,
        'id_user': values.id_user,
        'nama': values.nama,
        'no_telp': values.no_telp,
        'alamat': values.alamat,
        'longitude': values.longitude,
        'latitude': values.latitude
    }

    return data