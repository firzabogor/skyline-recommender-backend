from app.model.customer import Customer
from flask import request, jsonify
from app import response, db
from app.controller import UserController
from flask_jwt_extended import *


#@jwt_required
def index():
    try:
        customer = Customer.query.all()
        data = transform(customer)
        return response.ok(data, "")
    except Exception as e:
        print(e)

def store():
    try:
        id_user = request.json['id_user']
        kode_asal = request.json['kode_asal']
        nama = request.json['nama']
        no_telp = request.json['no_telp']
        alamat = request.json['alamat']

        customer = Customer(id_user=id_user, kode_asal=kode_asal, nama=nama, no_telp=no_telp, alamat=alamat)
        db.session.add(customer)
        db.session.commit()

        return response.ok('', 'Successfully create customer!')

    except Exception as e:
        print(e)


def update(id):
    try:
        id_user = request.json['id_user']
        kode_asal = request.json['kode_asal']
        nama = request.json['nama']
        no_telp = request.json['no_telp']
        alamat = request.json['alamat']

        customer = Customer.query.filter_by(id=id).first()
        customer.id_user = id_user
        customer.kode_asal = kode_asal
        customer.nama = nama
        customer.no_telp = no_telp
        customer.alamat = alamat

        db.session.commit()

        return response.ok('', 'Successfully update customer!')

    except Exception as e:
        print(e)


def show(id):
    try:
        customer = Customer.query.filter_by(id=id).first()
        if not customer:
            return response.badRequest([], 'Empty....')

        data = singleTransform(customer)
        return response.ok(data, "")
    except Exception as e:
        print(e)


def delete(id):
    try:
        customer = Customer.query.filter_by(id=id).first()
        if not customer:
            return response.badRequest([], 'Empty....')

        db.session.delete(customer)
        db.session.commit()

        return response.ok('', 'Successfully delete customer!')
    except Exception as e:
        print(e)


def transform(values):
    array = []
    for i in values:
        array.append(singleTransform(i))
    return array


def singleTransform(values, withUser=True):
    data = {
        'id': values.id,
        'id_user': values.id_user,
        'kode_asal': values.kode_asal,
        'nama': values.nama,
        'no_telp': values.no_telp,
        'alamat': values.alamat,
    }
    
    if withUser:
        user = []
        user.append({
            'id': values.users.id,
            'username': values.users.username,
            'email': values.users.email,
            'type_user': values.users.type_user,
        })  
        data['user'] = user

    return data