import mysql.connector
from mysql.connector import Error
from pandas import DataFrame as df
import pandas as pd
import gower
import numpy as np
from collections import Counter
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
from math import radians, cos, sin, asin, sqrt
from flask import request, jsonify
from pandasql import sqldf
import json

def skyline():
    try:
        connection = mysql.connector.connect(host='localhost',database='kuliner',user='root',password='')

        cursor = connection.cursor(buffered=True)

        id_type_makanan = str(request.json['id_type_makanan'])
        id_customer = str(request.json['id_customer'])
        user_longitude =  request.json['user_longitude']  
        user_latitude = request.json['user_latitude']

        numeric_column = list(['level_pedas','level_gurih','level_manis','level_asin','level_asam','nutrisi','tekstur_level','penggunaan_bahan_alami','kesegaran_makanan','kebersihan_makanan','status_halal_makanan','harga'])
        text_column = list(['bahan_bumbu','tekstur','aroma','rasa','cara_memasak','cara_penyajian'])

        def get_cosine_sim(*strs): 
            vectors = [t for t in get_vectors(*strs)]
            return cosine_similarity(vectors)

        def get_vectors(*strs):
            text = [t for t in strs]
            vectorizer = CountVectorizer(text)
            vectorizer.fit(text)
            return vectorizer.transform(text).toarray()

        def haversine(lon1, lat1, lon2, lat2):
            # convert decimal degrees to radians 
            lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

            # haversine formula 
            dlon = lon2 - lon1 
            dlat = lat2 - lat1 
            a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
            c = 2 * asin(sqrt(a)) 
            r = 6371 # Radius of earth in kilometers. Use 3956 for miles
            return c * r

        query_baseline_preferensi = "SELECT * FROM baseline_preferensi WHERE id_type_makanan = \""+id_type_makanan+"\" AND id_customer = \""+id_customer+"\" ORDER BY id"
        query_baseline_makanan = "SELECT * FROM baseline_makanan WHERE id_type_makanan = \""+id_type_makanan+"\" ORDER BY id"
        query_profil_makanan = """SELECT a.*,b.longitude,b.latitude, b.nama, b.alamat FROM profil_makanan a LEFT JOIN restoran b 
        ON a.id_restoran = b.id WHERE id_type_makanan = \""""+id_type_makanan+"""\" ORDER BY a.id"""

        cursor.execute(query_baseline_makanan)
        dataset_baseline_makanan = df(cursor.fetchall())
        dataset_baseline_makanan.columns = cursor.column_names

        cursor.execute(query_baseline_preferensi)
        dataset_baseline_preferensi = df(cursor.fetchall())
        dataset_baseline_preferensi.columns = cursor.column_names

        cursor.execute(query_profil_makanan)
        dataset_profil_makanan = df(cursor.fetchall())
        dataset_profil_makanan.columns = cursor.column_names

        skyline_candidate = pd.DataFrame(columns=['id_profil_makanan', 'longitude', 'latitude', 'p_gower', 'p_cosine', 'a_gower', 'a_cosine', 'distance'])
        skyline_candidate['id_profil_makanan'] = dataset_profil_makanan['id']
        skyline_candidate['longitude'] = dataset_profil_makanan['longitude']
        skyline_candidate['latitude'] = dataset_profil_makanan['latitude']
        skyline_candidate['alamat'] = dataset_profil_makanan['alamat']
        skyline_candidate['harga'] = dataset_profil_makanan['harga']
        skyline_candidate['nama'] = dataset_profil_makanan['nama']
        skyline_candidate['rating'] = dataset_profil_makanan['rating']
        skyline_candidate['hit'] = dataset_profil_makanan['hit']

        dataset_p_gower = pd.concat([dataset_baseline_preferensi[numeric_column],dataset_profil_makanan[numeric_column]]).reset_index(drop=True)
        dataset_p_gower = np.asarray(dataset_p_gower, dtype = float)
        p_gower_matrix = gower.gower_matrix(dataset_p_gower)[0]

        p_gower_array = []

        for i in range(len(dataset_p_gower)):
            if i != 0:
                p_gower_array.append(p_gower_matrix[i])

        for i in range(len(p_gower_array)):
            skyline_candidate.loc[skyline_candidate.index[i],'p_gower'] = p_gower_array[i]

        dataset_a_gower = pd.concat([dataset_baseline_makanan[numeric_column],dataset_profil_makanan[numeric_column]]).reset_index(drop=True)
        dataset_a_gower = np.asarray(dataset_a_gower, dtype = float)
        a_gower_matrix = gower.gower_matrix(dataset_a_gower)[0]
        a_gower_array = []

        for i in range(len(dataset_a_gower)):
            if i != 0:
                a_gower_array.append(a_gower_matrix[i])

        for i in range(len(a_gower_array)):
            skyline_candidate.loc[skyline_candidate.index[i],'a_gower'] = a_gower_array[i]


        dataset_p_cosine = pd.concat([dataset_baseline_preferensi[text_column],dataset_profil_makanan[text_column]]).reset_index(drop=True)

        dataset_p_cosine['text'] = dataset_p_cosine[dataset_p_cosine.columns[0:]].apply(lambda x: ','.join(x.dropna().astype(str)),axis=1)
        dataset_p_cosine['text'] = dataset_p_cosine['text'].str.replace(',', ' ')

        for i in range(len(dataset_p_cosine)):
            if i != 0 :
                skyline_candidate.loc[skyline_candidate.index[i-1],'p_cosine'] = 1-get_cosine_sim(dataset_p_cosine['text'].iloc[0],dataset_p_cosine['text'].iloc[i])[0][1]

        dataset_a_cosine = pd.concat([dataset_baseline_makanan[text_column],dataset_profil_makanan[text_column]]).reset_index(drop=True)

        dataset_a_cosine['text'] = dataset_a_cosine[dataset_a_cosine.columns[0:]].apply(lambda x: ','.join(x.dropna().astype(str)),axis=1)
        dataset_a_cosine['text'] = dataset_a_cosine['text'].str.replace(',', ' ')

        for i in range(len(dataset_a_cosine)):
            if i != 0 :
                skyline_candidate.loc[skyline_candidate.index[i-1],'a_cosine'] = 1-get_cosine_sim(dataset_a_cosine['text'].iloc[0],dataset_a_cosine['text'].iloc[i])[0][1]

        for i in range(len(skyline_candidate)):
            distance = haversine(user_longitude, user_latitude, skyline_candidate['longitude'].loc[i], skyline_candidate['latitude'].loc[i]);
            skyline_candidate.loc[skyline_candidate.index[i],'distance'] = distance

        skyline_candidate['cosine'] = skyline_candidate['a_cosine']+skyline_candidate['p_cosine']
        skyline_candidate['gower'] = skyline_candidate['a_gower']+skyline_candidate['p_gower']

        #del skyline_candidate['longitude']
        #del skyline_candidate['latitude']
        del skyline_candidate['p_gower']
        del skyline_candidate['p_cosine']
        del skyline_candidate['a_gower']
        del skyline_candidate['a_cosine']

        query = "SELECT * FROM skyline_candidate s WHERE s.distance < 6 AND NOT EXISTS(SELECT * FROM skyline_candidate s1 WHERE s1.gower <= s.gower AND s1.cosine <= s.cosine AND s1.distance <= s.distance AND (s1.gower < s.gower OR s1.cosine < s.cosine OR s1.distance < s.distance))"
        skyline = sqldf(query, {**locals(), **globals()})
        
        #query2 = "SELECT * FROM skyline WHERE distance <= 5"
        #skyline2 = sqldf(query2, {**locals(), **globals()})

        data = {}

        values = []
        for index, row in skyline.iterrows():
            values.append({
                'id_profil_makanan' : int(row['id_profil_makanan']),
                'distance' : row['distance'],
                'cosine' : row['cosine'],
                'gower' : row['gower'],
                'longitude' : row['longitude'],
                'latitude' : row['latitude'],
                'alamat' : row['alamat'],
                'harga' : row['harga'],
                'nama' : row['nama'],
                'rating' : row['rating'],
                'hit' : row['hit']
            })
        data['values'] = values

        return data

    except Error as e:
        print("Error reading data from MySQL table", e)