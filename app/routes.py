from app import app
from app.controller import UserController
from app.controller import WilayahController
from app.controller import CustomerController
from app.controller import RestoranController
from app.controller import TypeMakananController
from app.controller import BaselineMakananController
from app.controller import BaselinePreferensiController
from app.controller import ProfilMakananController
from app.controller import HistoriMakananController
from app.controller import SkylineController
from flask import request


@app.route('/api/users', methods=['POST', 'GET'])
def users():
    if request.method == 'GET':
        return UserController.index()
    else:
        return UserController.store()

@app.route('/api/users/<id>', methods=['PUT', 'GET', 'DELETE'])
def usersDetail(id):
    if request.method == 'GET':
        return UserController.show(id)
    elif request.method == 'PUT':
        return UserController.update(id)
    elif request.method == 'DELETE':
        return UserController.delete(id)

@app.route('/api/users/username', methods=['POST'])
def username():
    return UserController.show_username()

@app.route('/login', methods=['POST'])
def login():
    return UserController.login()

@app.route('/login2', methods=['POST'])
def login2():
    return UserController.login2()

@app.route('/customer', methods=['POST', 'GET'])
def customer():
    if request.method == 'GET':
        return CustomerController.index()
    else:
        return CustomerController.store()

@app.route('/customer/<id>', methods=['PUT', 'GET', 'DELETE'])
def customerDetail(id):
    if request.method == 'GET':
        return CustomerController.show(id)
    elif request.method == 'PUT':
        return CustomerController.update(id)
    elif request.method == 'DELETE':
        return CustomerController.delete(id)

@app.route('/wilayah', methods=['POST', 'GET'])
def wilayah():
    if request.method == 'GET':
        return WilayahController.index()
    else:
        return WilayahController.store()

@app.route('/wilayah/<kode>', methods=['PUT', 'GET', 'DELETE'])
def wilayahDetail(kode):
    if request.method == 'GET':
        return WilayahController.show(kode)
    elif request.method == 'PUT':
        return WilayahController.update(kode)
    elif request.method == 'DELETE':
        return WilayahController.delete(kode)

@app.route('/restoran', methods=['POST', 'GET'])
def restoran():
    if request.method == 'GET':
        return RestoranController.index()
    else:
        return RestoranController.store()

@app.route('/restoran/<id>', methods=['PUT', 'GET', 'DELETE'])
def restoranDetail(id):
    if request.method == 'GET':
        return RestoranController.show(id)
    elif request.method == 'PUT':
        return RestoranController.update(id)
    elif request.method == 'DELETE':
        return RestoranController.delete(id)

@app.route('/typemakanan', methods=['POST', 'GET'])
def typemakanan():
    if request.method == 'GET':
        return TypeMakananController.index()
    else:
        return TypeMakananController.store()

@app.route('/typemakanan/<id>', methods=['PUT', 'GET', 'DELETE'])
def typemakanDetail(id):
    if request.method == 'GET':
        return TypeMakananController.show(id)
    elif request.method == 'PUT':
        return TypeMakananController.update(id)
    elif request.method == 'DELETE':
        return TypeMakananController.delete(id)

@app.route('/baselinemakanan', methods=['POST', 'GET'])
def baselinemakanan():
    if request.method == 'GET':
        return BaselineMakananController.index()
    else:
        return BaselineMakananController.store()

@app.route('/baselinemakanan/<id>', methods=['PUT', 'GET', 'DELETE'])
def baselinemakananDetail(id):
    if request.method == 'GET':
        return BaselineMakananController.show(id)
    elif request.method == 'PUT':
        return BaselineMakananController.update(id)
    elif request.method == 'DELETE':
        return BaselineMakananController.delete(id)

@app.route('/baselinepreferensi', methods=['POST', 'GET'])
def baselinepreferensi():
    if request.method == 'GET':
        return BaselinePreferensiController.index()
    else:
        return BaselinePreferensiController.store()

@app.route('/insertupdatepreferensi', methods=['POST'])
def insertupdatepreferensi():
    if request.method == 'POST':
        return BaselinePreferensiController.insertupdatepreferensi()

@app.route('/baselinepreferensi/<id>', methods=['PUT', 'GET', 'DELETE'])
def baselinepreferensiDetail(id):
    if request.method == 'GET':
        return BaselinePreferensiController.show(id)
    elif request.method == 'PUT':
        return BaselinePreferensiController.update(id)
    elif request.method == 'DELETE':
        return BaselinePreferensiController.delete(id)

@app.route('/profilmakanan', methods=['POST', 'GET'])
def profilmakanan():
    if request.method == 'GET':
        return ProfilMakananController.index()
    else:
        return ProfilMakananController.store()

@app.route('/rating/<id>', methods=['PUT'])
def rating(id):
    if request.method == 'PUT':
        return ProfilMakananController.rating(id)

@app.route('/bytype/<id>', methods=['GET'])
def bytype(id):
    if request.method == 'GET':
        return ProfilMakananController.byType(id)

@app.route('/byrating/<id>', methods=['GET'])
def byrating(id):
    if request.method == 'GET':
        return ProfilMakananController.byRating(id)

@app.route('/profilmakanan/<id>', methods=['PUT', 'GET', 'DELETE'])
def profilmakananDetail(id):
    if request.method == 'GET':
        return ProfilMakananController.show(id)
    elif request.method == 'PUT':
        return ProfilMakananController.update(id)
    elif request.method == 'DELETE':
        return ProfilMakananController.delete(id)

@app.route('/historimakanan', methods=['POST', 'GET'])
def historimakanan():
    if request.method == 'GET':
        return HistoriMakananController.index()
    else:
        return HistoriMakananController.store()

@app.route('/historimakanan/<id>', methods=['PUT', 'GET', 'DELETE'])
def historimakananDetail(id):
    if request.method == 'GET':
        return HistoriMakananController.show(id)
    elif request.method == 'PUT':
        return HistoriMakananController.update(id)
    elif request.method == 'DELETE':
        return HistoriMakananController.delete(id)

@app.route('/skyline', methods=['POST'])
def skyine():
    if request.method == 'POST':
        return SkylineController.skyline()

@app.route('/refresh', methods=['POST'])
def refresh():
    return UserController.refresh()