from flask import Flask
from flask_cors import CORS
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_seeder import FlaskSeeder
from flask_jwt_extended import JWTManager

app = Flask(__name__)
CORS(app)
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
jwt = JWTManager(app)

seeder = FlaskSeeder()
seeder.init_app(app, db)

from app.model import user, wilayah, customer, restoran, typemakanan, baselinemakanan, baselinepreferensi, profilmakanan
from app import routes