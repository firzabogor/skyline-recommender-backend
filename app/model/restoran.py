from app import db
from app.model.user import Users
from sqlalchemy.orm import relationship


class Restoran(db.Model):
    __tablename__ = 'restoran'
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    nama = db.Column(db.String(230), nullable=False)
    no_telp = db.Column(db.String(30), nullable=False)
    alamat = db.Column(db.String(128), nullable=False)
    longitude = db.Column(db.Float, nullable=False)
    latitude = db.Column(db.Float, nullable=False)
    id_user = db.Column(db.BigInteger, db.ForeignKey(Users.id))

    users = relationship("Users", back_populates="restoran")

    def __repr__(self):
        return '<Restoran {}>'.format(self.nama)