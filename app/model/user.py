from app import db
from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash

class Users(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    username = db.Column(db.String(230), nullable=False)
    email = db.Column(db.String(120), index=True, unique=True, nullable=False)
    password = db.Column(db.String(128), nullable=False)
    type_user = db.Column(db.String(1),nullable=False)

    customer = db.relationship("Customer", uselist=False, back_populates="users")
    restoran = db.relationship("Restoran", uselist=False, back_populates="users")
    #customer = relationship("Customer", back_populates="users")
    #customers = db.relationship("Customer", lazy='select', backref=db.backref('customer', lazy='joined'))

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def setPassword(self, password):
        self.password = generate_password_hash(password)

    def checkPassword(self, password):
        return check_password_hash(self.password, password)