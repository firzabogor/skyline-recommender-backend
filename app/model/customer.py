from app import db
from app.model.user import Users
from app.model.wilayah import Wilayah
from sqlalchemy.orm import relationship


class Customer(db.Model):
    __tablename__ = 'customer'
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    kode_asal = db.Column(db.String(5), db.ForeignKey(Wilayah.kode))
    nama = db.Column(db.String(230), nullable=False)
    no_telp = db.Column(db.String(30), nullable=False)
    alamat = db.Column(db.String(128), nullable=False)
    id_user = db.Column(db.BigInteger, db.ForeignKey(Users.id))

    #users = relationship("Users", back_populates="customer")
    users = relationship("Users", back_populates="customer")

    #users = db.relationship("Users", backref="id_user")
    #asal = db.relationship("Wilayah", backref="kode_asal")

    def __repr__(self):
        return '<Customer {}>'.format(self.nama)