from app import db
from datetime import datetime
from app.model.profilmakanan import ProfilMakanan


class HistoriMakanan(db.Model):
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    waktu_pemesanan = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    id_profil_makanan = db.Column(db.BigInteger, db.ForeignKey(ProfilMakanan.id))
    rating = db.Column(db.Integer, nullable=False)

    profilmakanan = db.relationship("ProfilMakanan", backref="historiMakanan")

    def __repr__(self):
        return '<HistoriMakanan {}>'.format(self.rating)