from app import db
from app.model.typemakanan import TypeMakanan

class BaselineMakanan(db.Model):
    __tablename__ = 'baseline_makanan'
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    id_type_makanan = db.Column(db.BigInteger, db.ForeignKey(TypeMakanan.id))
    bahan_bumbu = db.Column(db.Text, nullable=False)
    tekstur = db.Column(db.Text, nullable=False)
    aroma = db.Column(db.Text, nullable=False)
    rasa = db.Column(db.Text, nullable=False)
    cara_memasak = db.Column(db.Text, nullable=False)
    cara_penyajian = db.Column(db.Text, nullable=False)
    level_pedas = db.Column(db.Integer, nullable=False)
    level_gurih = db.Column(db.Integer, nullable=False)
    level_manis = db.Column(db.Integer, nullable=False)
    level_asin = db.Column(db.Integer, nullable=False)
    level_asam = db.Column(db.Integer, nullable=False)
    nutrisi = db.Column(db.Integer, nullable=False)
    tekstur_level = db.Column(db.Integer, nullable=False)
    penggunaan_bahan_alami = db.Column(db.Integer, nullable=False)
    kesegaran_makanan = db.Column(db.Integer, nullable=False)
    kebersihan_makanan = db.Column(db.Integer, nullable=False)
    status_halal_makanan = db.Column(db.Integer, nullable=False)
    harga = db.Column(db.Integer, nullable=False)

    type_makanan = db.relationship("TypeMakanan", back_populates="baseline_makanan")

    def __repr__(self):
        return '<BaselineMakanan {}>'.format(self.bahan_bumbu)