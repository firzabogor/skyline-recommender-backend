from app import db
from app.model.wilayah import Wilayah

class TypeMakanan(db.Model):
    __tablename__ = 'type_makanan'
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    nama = db.Column(db.String(300), nullable=False)
    kode_asal = db.Column(db.String(5), db.ForeignKey(Wilayah.kode))

    wilayah = db.relationship("Wilayah", back_populates="type_makanan")

    baseline_makanan = db.relationship("BaselineMakanan", uselist=True, back_populates="type_makanan")

    def __repr__(self):
        return '<TypeMakanan {}>'.format(self.id)