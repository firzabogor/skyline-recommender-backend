from app import db

class Wilayah(db.Model):
    __tablename__ = 'wilayah'
    kode = db.Column(db.String(5), primary_key=True, nullable=False)
    nama = db.Column(db.String(300), nullable=False)

    type_makanan = db.relationship("TypeMakanan", uselist=False, back_populates="wilayah")

    #typemakanan = db.relationship("TypeMakanan", lazy='select', backref=db.backref('wilayah', lazy='joined'))
    #baselinemakanan = db.relationship("BaselineMakanan", lazy='select', backref=db.backref('wilayah', lazy='joined'))

    def __repr__(self):
        return '<Wilayah {}>'.format(self.kode)